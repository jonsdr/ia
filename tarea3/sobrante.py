from funciones import cruza, mutacion, est, selection
import random
import numpy as np

        
 
def ag5 (x,f,r,t,T):
    n = len(x)
    x1 = x.copy()
    win = est(x1,f)
    h = cruza(win)
    h1 = mutacion(h,t,T,r)
    p = np.concatenate((h1,x1), axis = 0)
    y = selection(p,f)
    
    
    return y               
 

