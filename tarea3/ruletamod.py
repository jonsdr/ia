from funciones import cruza, mutacion, jer, selection
import random
import numpy as np

def ag4 (x,f,r,t,T):
    n = len(x)
    x1 = x.copy()
    win = jer(x1,f)
    h = cruza(win)
    h1 = mutacion(h,t,T,r)
    p = np.concatenate((h1,x1), axis = 0)
    y = selection(p,f)
    
    
    return y               
        
