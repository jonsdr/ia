from funciones import cruza,mutacion,jer,est,torneod,selection,michalewicz
from funciones import h,g,f1,f2
import numpy as np
import math




def mutacion2(x,t,T,ran):
    n = len(x)
    mutados = np.zeros((n,len(x[0])))
    for i in range(n):
        a = x[i]
        k = np.random.randint(0,len(a))
        r = np.random.randint(0,2)
        vk = a[k]
        if r == 1:
        
            r1 = np.random.uniform()
            y = ran[1] - vk
            z = y*(1- pow(r1,pow(1-(t/T),5)))
            vk = vk + z
        else:
            r1 = np.random.uniform()
            y = vk - ran[0]
            z = y*(1- pow(r1,pow(1-(t/T),5)))
            vk = vk - z
        a[k] = vk
        mutados[i] = a
    return mutados

r = [-5,5]
n = 10
p0 = np.random.uniform(r[0], r[1], (n,5))
#y = jer(p0,f2)
x = [2.2098,1.5707,1.2830,1.9225,1.7197]
fx = michalewicz(x)
print(fx)
