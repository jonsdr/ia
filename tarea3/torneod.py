from funciones import cruza, mutacion, torneod, selection,f1,f2
import numpy as np
import random
import math


def ag1 (x,f,r,t,T):
    n = len(x)
    win = torneod(x,f)
    h = cruza(win)
    h1 = mutacion(h,t,T,r)
    p = np.concatenate((h1,x), axis = 0)
    y = selection(p,f)
    
    
    return y



r1 = [-1,1]
r2 = [0,math.pi]
n = 100
T = 500
p0 = np.random.uniform(r1[0], r1[1], (n,5))
p1 = np.random.uniform(r2[0], r2[1], (n,5))
a = p0
b = p1

for t in range(T):
    y = ag1(a,f1,r2,t,T)
    a = y

