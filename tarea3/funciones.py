import numpy as np
import random
import math

def h(x):
  z = x[0]**2 + x[1]**2 
  return z
  
def g(x):
  z = 100*(x[1] - x[0]**2)**2 + (1-x[0])**2
  return z

def f1(x):
   fx = 0
   for i in range (len(x)):
     fx = pow(abs(x[i]),i+1) + fx
   return fx 

def f2(x):
    n = len(x)
    result = 0.0
    for i in range(1, n + 1):
        result -= np.sin(x[i - 1]) * np.sin(i * x[i - 1]**2 / np.pi)**20
    return result

def cruza(x):
  n = len(x)
  h = np.zeros((n,len(x[0])))
  for i in range(0,n-1,2):
    p1 = x[i].copy()
    p2 = x[i+1].copy()
    h1 = x[i].copy()
    h2 = x[i+1].copy()

    h1[-1] = p2[-1]
    h2[-1] = p1[-1]

    h[i] = h1
    h[i+1] = h2
  return h

#mutacion uniforme
def mutacion(x,t,T,ran):
    n = len(x)
    mutados = np.zeros((n,len(x[0])))
    for i in range(n):
        a = x[i]
        k = np.random.randint(0,len(a))
        r = np.random.randint(0,2)
        vk = a[k]
        if r == 1:
        
            r1 = np.random.uniform()
            y = ran[1] - vk
            z = y*(1- pow(r1,pow(1-(t/T),5)))
            vk = vk + z
        else:
            r1 = np.random.uniform()
            y = vk - ran[0]
            z = y*(1- pow(r1,pow(1-(t/T),5)))
            vk = vk - z
        a[k] = vk
        mutados[i] = a
    return mutados
#torneo determinista
def torneod(x,f):
  
  n = len(x)
  mk = x.copy()
  y = np.zeros((n,len(x[0])))
  j = 0
  m = 0
  while j<2:
    perm = np.random.permutation(n)
    mk = mk[perm]
    for i in range(0,n-1,2):
      index = round(i/2)
      a = f(mk[i])
      b = f(mk[i+1])
      if (a<b):
        y[m + index] = mk[i] 
      else:
        y[m + index] = mk[i+1]
    m = round(n/2)
    j += 1
  return y

#torneo probabilistico
def torneop(x,f):
  
  n = len(x)
  mk = x.copy()
  y = np.zeros((n,len(x[0])))
  j = 0
  m = 0
  while j<2:
    perm = np.random.permutation(n)
    mk = mk[perm]
    for i in range(0,n-1,2):
      index = round(i/2)
      a = f(mk[i])
      b = f(mk[i+1])
      r = np.random.rand()
      if (r<.5):
        y[m + index] = mk[i] 
      else:
        y[m + index] = mk[i+1]
    m = round(n/2)
    j += 1
  return y 

#ruleta
def ruleta(x,f):
  n = len(x)
  fx = 0
  for i in range(n):
    fx = f(x[i])

  fx = fx/n
  ei = np.zeros(n)

  for i in range(n):
    ei[i] = f(x[i])/fx

  T = sum(ei)
  y = np.zeros((n,len(x[1])))
  for j in range(n):
    r = np.random.uniform(0,T)
    suma = 0
    for i in range(n):
      suma = ei[i] + suma
      if (suma > r):
        y[j] = x[i]
  return y

#sobrante estocastico
def est(x,f):
  n = len(x)
  x1 = x.copy()
  fx = 0
  ei = np.zeros(n)
  padres = np.empty((0,len(x[0])))
  decimales = np.zeros(n)
  for i in range(n):
    fx = f(x[i])

  fx = fx/n
  for i in range(n):
    ei[i] = f(x[i])/fx
    dec,ent = math.modf(ei[i])
    decimales[i] = dec
    if (ent!=0):
      padres = np.concatenate([padres, [x1[i]]], axis=0)
  while(len(padres)<n):

    for j in range(n):
      r = np.random.rand()
      if (r<decimales[j]):
        padres = np.concatenate([padres, [x1[j]]], axis=0)
        break


  return padres

#ruleta modificada
def jer(x,f):
  n = len(x)
  x1 = x.copy()
  ev = np.zeros(n)
  ei = np.zeros(n)
  for i in range(n):
    ev[i] = f(x[i])
    
  indexes = sorted(range(len(ev)), key=lambda i: ev[i])
  a = x1[indexes] 
  maximo = np.random.uniform(1,2)
  minimo = 2 - maximo

  for i in range(n):
    jerarquia = np.where(a == x[i])[0] 
    jerarquia = jerarquia[0]
    ei[i] = minimo + ((maximo - minimo)*(1/n-1)*(jerarquia - 1))

  T = sum(ei)
  y = np.zeros((n,len(x[1])))
  for j in range(n):
    r = np.random.uniform(0,T)
    suma = 0
    for i in range(n):
      suma = ei[i] + suma
      if (suma > r):
        y[j] = x[i]
  return y



#seleccion
def selection(x,f):
  n = len(x)
  m = round(n/2)
  ev = np.zeros(n)
  y = np.zeros((m,len(x[0])))
  for i in range(n):
    ev[i] = f(x[i])
    
  indexes = sorted(range(len(ev)), key=lambda i: ev[i])
  a = x[indexes]
  for i in range(m):
    y[i] = a[i]
  
  return y 
  

  
