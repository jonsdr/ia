from funciones import f1,cruza, mutacion, torneop, selection
import numpy as np
import random


def ag2 (x,f,r,t,T):
    n = len(x)
    x1 = x.copy()
    win = torneop(x1,f)
    h = cruza(win)
    h1 = mutacion(h,t,T,r)
    p = np.concatenate((h1,x1), axis = 0)
    y = selection(p,f)
    
    
    return y

