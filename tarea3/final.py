from funciones import f1,f2
from torneod import ag1
from torneop import ag2
from ruleta import ag3
from ruletamod import ag4
from sobrante import ag5
import math
import numpy as np

def final(x,f,ag,r):
    n = len(x)
    T = 500
    a = x.copy()
    best = np.zeros((30,5))
    for i in range(30):
        #obtener lista final
        for t in range(T):
            y = ag(a,f,r,t,T)
            a = y

        ev = np.zeros(n)
        for j in range(n):
            ev[j] = f(y[j])

        index = sorted(range(len(ev)), key=lambda i: ev[i])

        x11 = y.copy()
        x11 = x11[index]
        best[i] = x11[0]

    ev1 = np.zeros(30)
    fx=0
    for k in range(30):
        e = f(best[k]) 
        ev1[k] = e
        fx = fx + e
    
    fx = fx/30
    index1 = sorted(range(len(ev1)), key=lambda i: ev1[i]) 

    b = best.copy()
    b = b[index1]

    bb=b[0]
    ww=b[-1]
    
    return bb,ww,fx
r1 = [-1,1]
r2 = [0,math.pi]
n = 100
T = 500
p0 = np.random.uniform(r1[0], r1[1], (n,5))
p1 = np.random.uniform(r2[0], r2[1], (n,5))


# Bloque para det1
det1 = final(p0, f1, ag1, r1)
print("\nmejor det1\n{}\npeor det1\n{}\npromedio det1\n{}".format(det1[0], det1[1], det1[2]))

# Bloque para det2
det2 = final(p1, f2, ag1, r2)
print("\nmejor det2\n{}\npeor det2\n{}\npromedio det2\n{}".format(det2[0], det2[1], det2[2]))

# Bloque para prob1
prob1 = final(p0, f1, ag2, r1)
print("\nmejor prob1\n{}\npeor prob1\n{}\npromedio prob1\n{}".format(prob1[0], prob1[1], prob1[2]))

prob2 = final(p1, f2, ag2, r2)
print("\nmejor prob2\n{}\npeor prob2\n{}\npromedio prob2\n{}".format(prob2[0], prob2[1], prob2[2]))

ruleta1 = final(p0, f1, ag3, r1)
print("\nmejor ruleta1\n{}\npeor ruleta1\n{}\npromedio ruleta1\n{}".format(ruleta1[0], ruleta1[1], ruleta1[2]))

ruleta2 = final(p1, f2, ag3, r2)
print("\nmejor ruleta2\n{}\npeor ruleta2\n{}\npromedio ruleta2\n{}".format(ruleta2[0], ruleta2[1], ruleta2[2]))

ruletamod1 = final(p0, f1, ag4, r1)
print("\nmejor ruletamod1\n{}\npeor ruletamod1\n{}\npromedio ruletamod1\n{}".format(ruletamod1[0], ruletamod1[1], ruletamod1[2]))

ruletamod2 = final(p1, f2, ag4, r2)
print("\nmejor ruletamod2\n{}\npeor ruletamod2\n{}\npromedio ruletamod2\n{}".format(ruletamod2[0], ruletamod2[1], ruletamod2[2]))

sobrante1 = final(p0, f1, ag5, r1)
print("\nmejor sobrante1\n{}\npeor sobrante1\n{}\npromedio sobrante1\n{}".format(sobrante1[0], sobrante1[1], sobrante1[2]))

sobrante2 = final(p1, f2, ag5, r2)
print("\nmejor sobrante2\n{}\npeor sobrante2\n{}\npromedio sobrante2\n{}".format(sobrante2[0], sobrante2[1], sobrante2[2]))
