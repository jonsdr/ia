import numpy as np
import random

def g(x):
  z = 100*(x[1] - x[0]**2)**2 + (1-x[0])**2
  return z

#cruza simple
def cruza(p1,p2):
  h1,h2= p1,p2
  h1[-1:],h2[-1:] = p2[-1:], p1[-1:]
  return h1, h2

def torneo(x,f):
    n = len(x)
    perm = np.random.permutation(x.shape[0])
    mk = x[perm] 
    win = np.zeros(round(n/2))
    for i in range(0,n-1,2):
        a = f(mk[i])
        b = f(mk[i-1])
        index = round(i/2)
        #criterio de funcion
        if (a<b):
            win[index] = perm[i]
        else:
            win[index] = perm[i-1]
    return win
def cambio(y,r,t,T):
  x = y*(1 - pow(r,pow(1-(t/T),5)))
  return x

def mutacion(x,t,T,vb):
    n = len(x)
    r = random.randint(0,1)
    i = random.randint(0,n-1)
    vk = x[i]
    if r == 1:
        y = vb - vk
        vk = vk + cambio(y,i,n)
    else:
        y = vk - vb
        vk = vk - cambio(y,i,n)
    x[i] = vk
    return x

def ags(x,f):
  n = len(x)
  y = np.zeros((2*n,len(x[0])))
  for i in range(n):
    y[i] = x[i]
  for i in range(0,n,2):
    h1,h2 = cruza(x[i],x[i+1])
    y[n+i] = h1
    y[n+i+1] = h2
  a = torneo(y,f)
  index = [round(i) for i in a]
  win = y[index]
  return win

n = 10
p0 = np.round(np.random.rand(n,2)*100, 2)
print("poblacion inicial\n",p0)
#a = ags(p0,g)
#print("Proxima generacion\n",a)


a = toneo(p0)
print(a)




