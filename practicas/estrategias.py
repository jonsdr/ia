import numpy as np
import random

def f(x,y):
  z = x**2 + y**2
  return z

def g(x,y):
  z = 100*(y - x**2)**2 + (1-x)**2
  return z

def uno(x,f):
  n = len(x)
  ev = np.zeros(n)
  a=0
  b = x[0]
  #crear lista de evaluaciones y buscar al "peor"
  for i in range(n):
    fx = x[i]
    peor = f(b[0],b[1])
    ev[i] = f(fx[0],fx[1])

    if (ev[i] > peor):
        a = i #posicion del peor padre
        b = x[i] #el peor padre para iterar
  #print("peor padre es {} en la posicion {} \n".format(b,a))

  index = random.randint(0,n-1)
  cand = x[index]
  fcan = f(cand[0],cand[1])
  cand2 = cand + np.random.randn(2)
  fa = f(cand2[0],cand2[1])

  if (fa<fcan):
      x[a] = cand2

  return x

t = 1000 #generaciones
n = 10 #poblacion
p0 = np.round(np.random.rand(n,2)*100, 2)
print("primera funcion")
print("poblacion original ")
print(p0)
print()

a = uno(p0,f)

for i in range (t):
    x = uno(a,f)
    a = x
print("poblacion final\n" ,a)


p0 = np.round(np.random.rand(n,2)*100, 2)
print("segunda funcio")
print("poblacion original ")
print(p0)
print()

a = uno(p0,g)

for i in range (100000):
    x = uno(a,g)
    a = x
print("poblacion final\n" ,a)

