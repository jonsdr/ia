import numpy as np
import random

def h(x):
  z = x[0]**2 + x[1]**2 
  return z
  
def g(x):
  z = 100*(x[1] - x[0]**2)**2 + (1-x[0])**2
  return z



def ags(x,f):
  n = len(x)
  win = torneod(x,f)
  h = np.zeros((2*n,len(x[0])))
  for i in range(0,n-1,2):
    h1,h2 = cruza(win[i],win[i+1])
    h[i] = win[i]
    h[i+1] = win[i+1]
    h[n+i] = h1
    h[n+i+1] = h2
  
  return h

#cruza simple
def cruza(p1,p2):

  h1 = p1.copy()
  h2 = p2.copy()
  h1[-1] = p2[-1]
  h2[-1] = p1[-1]

  return h1, h2

#torneo determinista
def torneod(x,f):
    n = len(x)
    mk = x.copy()
    y = np.zeros((n,len(x[0])))
    j = 0
    m = 0
    while j<2:
      perm = np.random.permutation(x.shape[0])
      mk = mk[perm]
      for i in range(0,n-1,2):
        index = round(i/2)
        a = f(mk[i])
        b = f(mk[i+1])
        if (a<b):
          y[m + index] = mk[i] 
        else:
          y[m + index] = mk[i+1]
      m = round(n/2)
      j += 1
    return y



n = 10
r = [-5,5]
T = 25##numero final de generaciones 
p0 = np.random.uniform(r[0], r[1], (n,2))
print("poblacion inicial\n",p0)
a = ags(p0, g)
print("poblacion final\n",a)
