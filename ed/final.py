import numpy as np
import random
from funciones import f1,f2,f3
from edes import ed1,ed2,ed3
import math

def best(x,rango,ed,f):
    ev = np.zeros(len(x))
    for i in range(200):
        h = ed(x,rango,f)
        x = h
        
    for i in range(len(x)):
        ev[i] = f(x[i])
    y = x[np.argsort(ev)[0]]
    return y, f(y)

rango1 = [0,math.pi]
rango2 = [-1,1]
n = 100
ev = np.zeros(n)
p0 = np.random.uniform(rango1[0],rango1[1],(n,5))
p1 =np.random.uniform(rango2[0],rango2[1],(n,5))
p2 = np.random.uniform(rango2[0],rango2[1],(n,2))
    
print("ed 1")

print("funcion 1")
a = best(p1,rango2,ed1,f1)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])

print("funcion2")
a = best(p0,rango1,ed1,f2)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])

print("funcion3")
a = best(p2,rango2,ed1,f3)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])

##best##
print("ed best")

print("funcion 1")
a = best(p1,rango2,ed2,f1)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])

print("funcion2")
a = best(p0,rango1,ed2,f2)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])

print("funcion3")
a = best(p2,rango2,ed2,f3)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])

##target to best##
print("ed target to best")

print("funcion 1")
a = best(p1,rango2,ed3,f1)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])

print("funcion2")
a = best(p0,rango1,ed3,f2)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])

print("funcion3")
a = best(p2,rango2,ed3,f3)
print("mejor individuo\n", a[0], "\nf(x) = ", a[1])
