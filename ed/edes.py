import numpy as np
import random
from funciones import f1,f2,f3

def ed1(x,rango,f):
    n = len(x)
    m = len(x[0])
    aux = np.zeros((n,m))
    y = np.zeros((n,m))
    u = x.copy()
    evx = np.zeros(n)
    evh = np.zeros(n)
    x1 = x.copy()
    for i in range(n):
        evx[i] = f(x[i])

    F = np.random.rand()
    
    for i in range(n):
        tres = x1[np.random.choice(x1.shape[0],3,replace=False)]
        aux[i] = tres[0] + F * (tres[1] - tres[2])

    for i in range(n):
        r = np.random.rand()
        cr = .5    #probabilidad de cruza
        if r <= cr:
            u[i] = aux[i]
    for i in range(n):
        if f(u[i]) <= f(x1[i]):
            y[i] = u[i]
        else:
            y[i] = x1[i]

        
    
    return y

###best###
def ed2(x,rango,f):
    n = len(x)
    m = len(x[0])
    aux = np.zeros((n,m))
    y = np.zeros((n,m))
    u = x.copy()
    evx = np.zeros(n)
    evh = np.zeros(n)
    x1 = x.copy()
    for i in range(n):
        evx[i] = f(x[i])

    F = .5
    best = x1[np.argsort(evx)[0]]
    for i in range(n):
        tres = x1[np.random.choice(x1.shape[0],2,replace=False)]
        aux[i] = best + F * (tres[0] - tres[1])

    for i in range(n):
        r = np.random.rand()
        cr = .7    #probabilidad de cruza
        if r <= cr:
            u[i] = aux[i]
    for i in range(n):
        if f(u[i]) <= f(x1[i]):
            y[i] = u[i]
        else:
            y[i] = x1[i]

        
    
    return y

##target to best###
def ed3(x,rango,f):
    n = len(x)
    m = len(x[0])
    aux = np.zeros((n,m))
    y = np.zeros((n,m))
    u = x.copy()
    evx = np.zeros(n)
    evh = np.zeros(n)
    x1 = x.copy()
    for i in range(n):
        evx[i] = f(x[i])

    F = .5
    best = x1[np.argsort(evx)[0]]
    for i in range(n):
        tres = x1[np.random.choice(x1.shape[0],2,replace=False)]
        aux[i] = x1[i] + F * (best - x1[i]) + F *(tres[0] - tres[1])

    for i in range(n):
        r = np.random.rand()
        cr = .7    #probabilidad de cruza
        if r <= cr:
            u[i] = aux[i]
    for i in range(n):
        if f(u[i]) <= f(x1[i]):
            y[i] = u[i]
        else:
            y[i] = x1[i]

        
    
    return y


rango = [-1,1]
n = 10
p0 = np.random.uniform(rango[0],rango[1],(n,2))
#print(p0)
#y = p0[np.random.choice(p0.shape[0],3,replace=False)]
#print(y)

#y = ed3(p0,rango,f1)
#print(y)
#F = np.random.rand()



#mi_arreglo = np.array([3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5])

# Obtener los índices que ordenan el arreglo

# Obtener el índice original del primer elemento
#indice_original = np.argsort(mi_arreglo)[0]
#print(indice_original)
