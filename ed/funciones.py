import random
import math
import numpy as np

def f1(x):
   fx = 0
   for i in range (len(x)):
     fx = pow(abs(x[i]),i+1) + fx
   return fx 

def f2(x):
    n = len(x)
    result = 0.0
    for i in range(1, n + 1):
        result -= np.sin(x[i - 1]) * np.sin((i * x[i - 1]**2) / np.pi)**20
    return result

def f3(x):
  z = x[0]**2 + x[1]**2 
  return z
