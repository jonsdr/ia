import numpy as np
import math
import random 

def f(x):
    n = len(x)
    y = 0 
    for i in range(n):
        y += x[i]**2
    return y

def g(x):
    a = x[0] + x[1]
    y = math.sin(x[0] + x[1]) + (x[0] - x[1])**2 - 1.5*x[0] + 2.5*x[1] + 1
    return y

def h(x):
    n = len(x)
    y = 0
    for i in range(1,n):
        y += 100*(x[i] - x[i-1]**2)**2 + (x[i-1] -1)**2
    return y

def pe(x,T,vb,f):
    n = len(x)
    y = np.zeros((n*2,len(x[0])))
    win = x
    for t in range(T):
        for i in range(n):
            y[i] = win[i]
            y[n+i] = mutacion(x[i],t,T,vb)
        a = torneo(y,f)
        index = [round(i) for i in a]
        win = y[index]
    return win

def torneo(x,f):
    n = len(x)
    perm = np.random.permutation(x.shape[0])
    mk = x[perm] 
    win = np.zeros(round(n/2))
    for i in range(0,n-1,2):
        a = f(mk[i])
        b = f(mk[i-1])
        index = round(i/2)
        #criterio de funcion
        if (a<b):
            win[index] = perm[i]
        else:
            win[index] = perm[i-1]
    return win
def cambio(y,t,T):
    r = np.random.rand(1)
    x = y*(1 - pow(r,pow(1-(t/T),5)))
  return x

def mutacion(x,t,T,vb):
    n = len(x)
    r = random.randint(1)
    i = random.randint(n)
    vk = x[i]
    if r == 1:
        y = vb - vk
        vk = vk + cambio(y,i,n)
    else:
        y = vk - vb
        vk = vk - cambio(y,i,n)
    x[i] = vk
    return x




p0 = np.random.rand(10,2)*100
print(p0)
a = f(p0)
c = h(p0)

p0 = np.random.rand(2)*100
b = g(p0)
print(p0)
print(b)
