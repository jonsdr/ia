#programacion evolutiva
import numpy as np
import random
import math

def f(x):
    n = len(x)
    y = 0 
    for i in range(n):
        y += x[i]**2
    return y

def g(x):
    a = x[0] + x[1]
    y = math.sin(x[0] + x[1]) + (x[0] - x[1])**2 - 1.5*x[0] + 2.5*x[1] + 1
    return y

def h(x):
    n = len(x)
    y = 0
    for i in range(1,n):
        y += 100*(x[i] - x[i-1]**2)**2 + (x[i-1] -1)**2
    return y



def pe(x,T,vb,f):
    n = len(x)
    y = np.zeros((n*2,len(x[0])))
    win = x
    for t in range(T):
        for i in range(n):
            y[i] = win[i]
            y[n+i] = mutacion(x[i],t,T,vb)
        a = torneo(y,f)
        index = [round(i) for i in a]
        win = y[index]
    return win
def mutacion(x,t,T,vb):
    n = len(x)
    r = random.randint(0,1)
    i = random.randint(0,n-1)
    vk = x[i]
    if r == 1:
        y = vb - vk
        vk = vk + cambio(y,i,n)
    else:
        y = vk - vb
        vk = vk - cambio(y,i,n)
    x[i] = vk
    return x

def torneo(x,f):
    n = len(x)
    perm = np.random.permutation(x.shape[0])
    mk = x[perm] 
    win = np.zeros(round(n/2))
    for i in range(0,n-1,2):
        a = f(mk[i])
        b = f(mk[i-1])
        index = round(i/2)
        #criterio de funcion
        if (a<b):
            win[index] = perm[i]
        else:
            win[index] = perm[i-1]
    return win


def cambio(y,t,T):
    r = np.random.rand(1)
    x = y*(1 - pow(r,pow(1-(t/T),5)))
    return x

def pob2(l1,l2):
    x = np.zeros((50,2))
    for i in range (50):
        a = np.random.uniform(l1[0],l1[1])
        b = np.random.uniform(l2[0],l2[1])
        x[i] = [a,b]
    return x
    
    
    
    return x

genes = [5,10,15,20]
n = 50
T = 100
print("programacion evolutiva")
print("funcion 1")
for gen in genes:
    p0 = np.random.uniform(-5.12, 5.12, (n,gen))
    a = pe(p0,T,-5.12,f)
    print("poblacion original n = 50")
    print(p0)
    print("poblacion final despues de 100 generaciones")
    print(a)



print("funcion 2")
p0 = pob2([-1.5,4],[-3,4])
print("Poblacion original")
print(p0)
print("poblacion final despues de 100 generaciones")
a = pe(p0,T,-3,g)
print(a)


print("funcion 3")
for gen in genes:
    p0 = np.random.uniform(-5, 10, (n,gen))
    a = pe(p0,T,-5.12,h)
    print("poblacion original n = 50")
    print(p0)
    print("poblacion final despues de 100 generaciones")
    print(a)
