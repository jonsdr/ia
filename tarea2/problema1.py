import numpy as np
import random

def f(x):
    y = x[0]**2 + x[1]**2
    return y

def ruido(x):
    for i in range(len(x)):
        x[i] = x[i] + np.random.randn()
    return x 
def torneo(x,f):
    n = len(x)
    perm = np.random.permutation(x.shape[0])
    mk = x[perm] 
    win = np.zeros(round(n/2))
    for i in range(0,n-1,2):
        a = f(mk[i])
        b = f(mk[i-1])
        index = round(i/2)
        #criterio de funcion
        if (a<b):
            win[index] = perm[i]
        else:
            win[index] = perm[i-1]
    return win
                 
            
def ee(x,f):
    n = len(x)
    y = np.zeros((n*2,len(x[0])))
    fy = y
    
    #hijos y padres
    for i in range(n):
        y[i] = x[i]
        y[n+i] = ruido(x[i])
       #ganadores 
    a = torneo(y,f)
    index = [round(i) for i in a]
    win = y[index]

    return win

def ee2(x,f):
    n = len(x)
    y = np.zeros((n, len(x[0])))
    for i in range(n):
        y[i] =  ruido(x[i])
    return y 

#poblacion inicial
p0 = np.random.rand(10,2)*100
T = 50 #numero de generaciones deseadas
a = p0
print("mu + lambda")
print("Generacion: 1 ")
print(ee(p0,f))
for t in range (T):
    r = ee(a,f)
    a = r
                   
print("Generacion: ",T) 
print(a)

print("mu , lamda")
print ("Generacion: 1")
print(ee2(p0,f))
for t in range (T):
    r = ee2(a,f)
    a = r
                   
print("Generacion: ",T) 
print(a)

