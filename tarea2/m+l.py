import numpy as np
import random
import math

def f(x):
    n = len(x)
    y = 0 
    for i in range(n):
        y += x[i]**2
    return y

def g(x):
    a = x[0] + x[1]
    y = math.sin(x[0] + x[1]) + (x[0] - x[1])**2 - 1.5*x[0] + 2.5*x[1] + 1
    return y

def h(x):
    n = len(x)
    y = 0
    for i in range(1,n):
        y += 100*(x[i] - x[i-1]**2)**2 + (x[i-1] -1)**2
    return y


def ruido(x):
    for i in range(len(x)):
        x[i] = x[i] + np.random.randn()
    return x 
def torneo(x,f):
    n = len(x)
    perm = np.random.permutation(x.shape[0])
    mk = x[perm] 
    win = np.zeros(round(n/2))
    for i in range(0,n-1,2):
        a = f(mk[i])
        b = f(mk[i-1])
        index = round(i/2)
        #criterio de funcion
        if (a<b):
            win[index] = perm[i]
        else:
            win[index] = perm[i-1]
    return win
            
def ee(x,f):
    n = len(x)
    y = np.zeros((n*2,len(x[0])))
    fy = y
    
    #hijos y padres
    for i in range(n):
        y[i] = x[i]
        y[n+i] = ruido(x[i])
       #ganadores 
    a = torneo(y,f)
    index = [round(i) for i in a]
    win = y[index]

    return win

def pob2(l1,l2):
    x = np.zeros((1,2))
    for i in range (len(x)):
        a = np.random.uniform(l1[0],l1[1])
        b = np.random.uniform(l2[0],l2[1])
        x[i] = [a,b]
    return x
 
print("(m+l)--ee")

genes = [5,10,15,20]
n = 50
T = 100
print("funcion 1")
for gen in genes:
    p0 = np.random.uniform(-5.12, 5.12, (n,gen))
    a = ee(p0,f)
    print("poblacion original n = 1")
    print(p0)
    print("poblacion final despues de 100 generaciones")
    print(a)



print("funcion 2")
p0 = pob2([-1.5,4],[-3,4])
print("Poblacion original")
a = ee(p0,g)
print(p0)
print("poblacion final despues de 100 generaciones")
for i in range (100):
    b = ee(a,g)
    a = b 
    
print(a)


print("funcion 3")
for gen in genes:
    p0 = np.random.uniform(-5, 10, (n,gen))
    a = ee(p0,h)
    print("poblacion original n = 1")
    print(p0)
    print("poblacion final despues de 100 generaciones")
    for i in range (100):
        b = ee(a,h)
        a = b
    print(a)
