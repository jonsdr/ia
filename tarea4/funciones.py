import numpy as np
import random 


def peso(x,m):
    n = len(x)
    suma = 0
    for i in range(-1,n-1):
        a = x[i] - 1
        fila = m[a]
        suma = suma + fila[x[i+1]-1]
    return suma

def torneod(x,f,M):
  
  n = len(x)
  mk = x.copy()
  y = np.zeros((n,len(x[0])),dtype = int)
  j = 0
  m = 0
  while j<2:
    perm = np.random.permutation(n)
    mk = mk[perm]
    for i in range(0,n-1,2):
      index = round(i/2)
      a = f(mk[i],M)
      b = f(mk[i+1],M)
      if (a<b):
        y[m + index] = mk[i] 
      else:
        y[m + index] = mk[i+1]
    m = round(n/2)
    j += 1
  return y


###partially mapped crossover
def pmx(x):
    n = len(x)
    m = len(x[0])
    indices = sorted(random.sample(list(range(1,m)),2)) 
    h = np.zeros((n,m),dtype = int)
    for i in range(0,n-1,2):
        p1 = x[i].copy()
        p2 = x[i+1].copy()
        h1 = np.zeros(m,dtype= int)
        h2 = np.zeros(m,dtype= int)
        
        mapped = np.empty((0,2),dtype=int)
        for j in range(indices[0],indices[1]+1):
            h1[j] = p2[j]
            h2[j] = p1[j]
        for j in range(m):
            if h1[j]==0:
                if p1[j] not in h1:
                    h1[j] = p1[j]
                else:
                    index = np.where(h1 == p1[j])[0][0]
                    if h2[index] not in h1:
                        h1[j] = h2[index]
                    
                    else:
                        index2 = np.where(h1 == h2[index])[0][0]
                        h1[j] = h2[index2]

            if h2[j]==0:
                if p2[j] not in h2:
                    h2[j] = p2[j]
                else:
                    index3 = np.where(h2 == p2[j])[0][0]
                    if h1[index3] not in h2:
                        h2[j] = h1[index3]
                    
                    else:
                        index4 = np.where(h2 == h1[index3])[0][0]
                        h2[j] = h1[index4]
        h[i] = h1
        h[i+1] = h2
    return h
 
#position based crossover
def pbx(x):
    n = len(x)
    m = len(x[0])
    alpha = np.random.randint(2,m)
    indices = sorted(random.sample(list(range(1,m)),alpha)) 
    h = np.zeros((n,m),dtype = int)
    lista1 = np.empty(0)
    lista2 = np.empty(0)
    lista3 = np.empty(0)
    for i in range(0,n-1,2):
        p1 = x[i].copy()
        p2 = x[i+1].copy()
        h1 = x[i].copy()
        h2 = x[i+1].copy()
        s2 = x[i].copy()
        s1 = x[i+1].copy()

        
        for k in range(len(indices)-1):
            j = indices[k]
            while j <= indices[k+1]:
                lista1 = np.append(lista1,p1[j])
                lista2 = np.append(lista2,p2[j])
                lista3 = np.append(lista3,j)

                j = j + 1
        for j in range(m):
            for k in range(len(lista1)):
                if p1[j] not in lista1:
                    h1[j] = 0
                if p2[j] in lista1:
                    s1[j] = 0
                if p2[j] not in lista2:
                    h2[j]= 0
                if p1[j] in lista2:
                    s2[j] = 0

        k1 = 0
        k2 = 0
        for j in range(m):
            if h1[j] == 0:
                while s1[k1] == 0:
                    k1 = k1+1
                h1[j] = s1[k1]
                k1= k1+1
            if h2[j] == 0:
                while s2[k2] == 0:
                    k2 = k2+1
                h2[j] = s2[k2]
                k2= k2+1
        h[i] = h1
        h[i+1] = h2
    return h

def obx(x):
    n = len(x)
    m = len(x[0])
    alpha = np.random.randint(2,m)
    valores = sorted(random.sample(list(range(1,m)),alpha)) 
    h = np.zeros((n,m),dtype = int)

    for i in range (0,n-1,2):
        h1 = x[i].copy()
        h2 = x[i+1].copy()
        s1 = np.zeros(m)
        s2 = np.zeros(m)

        for j in range(m):
            if h1[j] not in valores:
                s1[j] = h1[j]
                h1[j] = 0
            if h2[j] not in valores:
                s2[j] = h2[j]
                h2[j] = 0
        k = 0
        k2 = 0
        for j in range(m):
            if h1[j] == 0:
                while s2[k] == 0:
                    k = k+1
                h1[j] = int(s2[k])
                k = k+1
                    
            if h2[j] == 0:
                while s1[k2] == 0:
                    k2 = k2+1
                h2[j] = int(s1[k2])
                k2 = k2+1
            
        h[i] = h1
        h[i+1] = h2
    
    return h



def evaluacion(x,m,f,seleccion,genetico):
    y = x.copy()
    for i in range(20):
        p = seleccion(y,f,m)
        h = genetico(p)
        y = h
    ev = np.zeros(len(x))
    for i in range(len(x)):
        ev[i] = f(y[i],m)
    minimo = min(ev)
    index = np.where(ev == minimo)[0]
    y1 = y[index[0]]
    return y1,int(minimo)
