import numpy as np
import random

def pmx(x):
    n = len(x)
    m = len(x[0])
    indices = sorted(random.sample(list(range(1,m)),2)) 
    print(indices)
    h = np.zeros((n,m),dtype = int)
    for i in range(0,n-1,2):
        p1 = x[i].copy()
        p2 = x[i+1].copy()
        h1 = np.zeros(m,dtype= int)
        h2 = np.zeros(m,dtype= int)
        
        mapped = np.empty((0,2),dtype=int)
        for j in range(indices[0],indices[1]+1):
            h1[j] = p2[j]
            h2[j] = p1[j]
        for j in range(m):
            if h1[j]==0:
                if p1[j] not in h1:
                    h1[j] = p1[j]
                else:
                    index = np.where(h1 == p1[j])[0][0]
                    if h2[index] not in h1:
                        h1[j] = h2[index]
                    
                    else:
                        index2 = np.where(h1 == h2[index])[0][0]
                        h1[j] = h2[index2]

            if h2[j]==0:
                if p2[j] not in h2:
                    h2[j] = p2[j]
                else:
                    index = np.where(h2 == p2[j])[0][0]
                    if h1[index] not in h2:
                        h2[j] = h1[index]
                    
                    else:
                        index2 = np.where(h2 == h1[index])[0][0]
                        h2[j] = h1[index2]
        h[i] = h1
        h[i+1] = h2
    return h
             
print('instancia 1')
costos = np.array([[0,2,1,10,25],
                   [2,0,18,5,18],
                   [1,18,0,10,20],
                   [10,5,10,0,7],
                   [25,18,20,7,0]])


x1 = np.array([1,2,3,4,5])
n1 = np.array([np.random.permutation(x1) for _ in range(10)])

print(n1)
a = pmx(n1)
print(a)
