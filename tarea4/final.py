import numpy as np
from funciones import evaluacion, pmx, obx, pbx, torneod, peso
# Lista original

##instancia 1
print('instancia 1')
costos = np.array([[0,2,1,10,25],
                   [2,0,18,5,18],
                   [1,18,0,10,20],
                   [10,5,10,0,7],
                   [25,18,20,7,0]])


x1 = np.array([1,2,3,4,5])
n1 = np.array([np.random.permutation(x1) for _ in range(10)])


a = evaluacion(n1,costos,peso,torneod,obx)
b = evaluacion(n1,costos,peso,torneod,pbx)
c = evaluacion(n1,costos,peso,torneod,pmx)
print("order base\nmejor camino encontrado\n",a[0],"\npeso",a[1])
print("position base\nmejor camino encontrado\n",b[0],"\npeso",b[1])
print("partially mapped \nmejor camino encontrado\n",c[0],"\npeso",c[1])

#instancia 2
print('instancia 2')
costos = np.array([[0,300,180,300,460,250],
                   [300,0,160,140,360,150],
                   [180,160,0,200,290,100],
                   [300,140,200,0,200,150],
                   [460,360,290,200,0,360],
                   [250,150,100,150,360,0]])

x1 = np.array([1,2,3,4,5,6])
n1 = np.array([np.random.permutation(x1) for _ in range(20)])


a = evaluacion(n1,costos,peso,torneod,obx)
b = evaluacion(n1,costos,peso,torneod,pbx)
c = evaluacion(n1,costos,peso,torneod,pmx)
print("order base\nmejor camino encontrado\n",a[0],"\npeso",a[1])
print("position base\nmejor camino encontrado\n",b[0],"\npeso",b[1])
print("partially mapped \nmejor camino encontrado\n",c[0],"\npeso",c[1])


#instancia 3
print("instancia 3")
costos = np.array([[0,386,294,249,214,413,171,327,237,129],
                   [386,0,266,273,312,276,367,575,287,30],
                   [294,266,0,110,259,245,185,186,114,70],
                   [249,273,110,0,259,18,464,373,398,451],
                   [214,312,259,259,0,332,78,650,245,499],
                   [413,276,245,18,332,0,140,54,105,485],
                   [171,367,185,464,78,140,0,448,336,312],
                   [327,575,186,373,650,54,448,0,260,322],
                   [237,287,114,398,245,105,336,260,0,382],
                   [129,30,70,451,499,485,312,322,382,0]])


x1 = np.array([1,2,3,4,5,6,7,8,9,10])
n1 = np.array([np.random.permutation(x1) for _ in range(30)])


a = evaluacion(n1,costos,peso,torneod,obx)
b = evaluacion(n1,costos,peso,torneod,pbx)
c = evaluacion(n1,costos,peso,torneod,pmx)
print("order base\nmejor camino encontrado\n",a[0],"\npeso",a[1])
print("position base\nmejor camino encontrado\n",b[0],"\npeso",b[1])
print("partially mapped \nmejor camino encontrado\n",c[0],"\npeso",c[1])
