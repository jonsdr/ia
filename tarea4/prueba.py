import numpy as np


def pmx(x):
    n = len(x)
    m = len(x[0])
    indices = sorted(random.sample(list(range(1,m)),2)) 
    h = np.zeros((n,m),dtype = int)
    for i in range(0,n-1,2):
        p1 = x[i].copy()
        p2 = x[i+1].copy()
        h1 = np.zeros(m,dtype= int)
        h2 = np.zeros(m,dtype= int)
        
        mapped = np.empty((0,2),dtype=int)
        for j in range(indices[0],indices[1]+1):
            h1[j] = p2[j]
            h2[j] = p1[j]
        for j in range(m):
            if h1[j]==0:
                if p1[j] not in h1:
                    h1[j] = p1[j]
                else:
                    index = np.where(h1 == p1[j])[0][0]
                    if h2[index] not in h1:
                        h1[j] = h2[index]
                    
                    else:
                        index2 = np.where(h1 == h2[index])[0][0]
                        h1[j] = h2[index2]

            if h2[j]==0:
                if p2[j] not in h2:
                    h2[j] = p2[j]
                else:
                    index3 = np.where(h2 == p2[j])[0][0]
                    if h1[index3] not in h2:
                        h2[j] = h1[index3]
                    
                    else:
                        index4 = np.where(h2 == h1[index3])[0][0]
                        h2[j] = h1[index4]
        h[i] = h1
        h[i+1] = h2
    return h
